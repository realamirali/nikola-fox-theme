# Nikola Fox theme
this is a nikola blog theme based on detox theme with gitlab colors design!  
This theme use `Behdad Font` by default!  
See live demo on [My Blog](https://amiralinull.gitlab.io/)

to use this:
- make a nikola project

```
$ nikola init blog
```

- clone this repository into themes directory:

```
$ git clone https://gitlab.com/AmiraliNull/nikola-fox-theme.git blog/themes/fox
```

- set `THEME` in `conf.py`:

```python
THEME = "fox"
```

- done!  
- build your blog!
